s --> np(Number), vp(Number).

np(Number) --> det(Number), n(Number).

vp(Number) --> v(Number), np(_).
vp(Number) --> v(Number).

det(Number) --> [Word], {lex(Word, Number, det)}.

v(Number) --> [Word], {lex(Word, Number, v)}.

n(Number) --> [Word], {lex(Word, Number, n)}.

lex(the, _, det).
lex(a, singular, det).

lex(eat, plural, v).
lex(eats, singular, v).
lex(know, plural, v).
lex(knows, singular, v).

lex(women, plural, n).
lex(woman, singular, n).
lex(men, plural, n).
lex(man, singular, n).
lex(apples, plural, n).
lex(apple, singular, n).
lex(pears, plural, n).
lex(pear, singular, n).

% Tests
:- s([the, men, eat], []).
:- s([the, man, eats], []).
:- \+(s([the, men, eats], [])).
:- \+(s([the, man, eat], [])).
