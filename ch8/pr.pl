:- set_prolog_flag(discontiguous_warnings, off).

% ------------------ %
% --- Question 1 --- %
% ------------------ %
s(s(NP, VP)) --> np(NP, Nb, subject), vp(VP, Nb, _).

np(np(DET, N), Nb, _) --> det(DET, Nb, _), n(N, Nb, _).
np(np(PRO), Nb, Fn) --> pro(PRO, Nb, Fn).

vp(vp(V, NP), Nb, _) --> v(V, Nb, _), np(NP, _, object).
vp(vp(V), Nb, _) --> v(V, Nb, _).

det(det(Word), Nb, _) --> [Word], {lex(Word, Nb, _, det)}.
v(v(Word), Nb, _) --> [Word], {lex(Word, Nb, _, v)}.
n(n(Word), Nb, _) --> [Word], {lex(Word, Nb, _, n)}.
pro(pro(Word), Nb, Fn) --> [Word], {lex(Word, Nb, Fn, pro)}.

lex(the, _, _, det).
lex(a, singular, _, det).

lex(eat, plural, _, v).
lex(eats, singular, _, v).
lex(know, plural, _, v).
lex(knows, singular, _, v).
lex(shoots, singular, _, v).
lex(shoot, plural, _, v).

lex(women, plural, _, n).
lex(woman, singular, _, n).
lex(men, plural, _, n).
lex(man, singular, _, n).
lex(apples, plural, _, n).
lex(apple, singular, _, n).
lex(pears, plural, _, n).
lex(pear, singular, _, n).

lex(he, singular, subject, pro).
lex(she, singular, subject, pro).
lex(him, singular, object, pro).
lex(her, singular, object, pro).

% Tests
:- s(_, [the, men, eat], []).
:- s(_, [the, man, eats], []).
:- \+(s(_, [the, men, eats], [])).
:- \+(s(_, [the, man, eat], [])).
:- s(_, [she, shoots, him], []).
:- \+(s(_, [a, woman, shoots, she], [])).
:- \+(s(_, [her, shoots, a, man], [])).
:- \+(s(_, [her, shoots, she], [])).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
np(np(DET, MOD, N, PP), _, _) --> det(DET, _, _), mod(MOD, _, _), n(N, _, _),
                                  pp(PP, _, _).

mod([], _, _) --> [].
mod(mod(ADJ, MOD), _, _) --> adj(ADJ, _, _), mod(MOD, _, _).

adj(adj(Word), _, _) --> [Word], {lex(Word, _, _, adj)}.

pp([], _, _) --> [].
pp(pp(PRE, NP), _, _) --> pre(PRE, _, _), np(NP, _, _).

pre(pre(Word), _, _) --> [Word], {lex(Word, _, _, pre)}.

lex(small, _, _, adj).
lex(frightened, _, _, adj).
lex(big, _, _, adj).
lex(fat, _, _, adj).

lex(table, singular, _, n).
lex(shower, singular, _, n).
lex(cow, singular, _, n).

lex(on, _, _, pre).
lex(under, _, _, pre).

:- np(_, _, _, [the, small, frightened, woman, on, the, table], []).
:- np(_, _, _, [the, big, fat, cow, under, the, shower], []).
