kanga(V, R, Q) --> roo(V, R), jumps(Q, Q), {marsupial(V, R, Q)}.

kanga2(V, R, Q, Di, Df) :- roo(V, R, Di, Dii), jumps(Q, Q, Dii, Df),
                           marsupial(V, R, Q).
