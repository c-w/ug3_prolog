% ------------------ %
% --- Question 1 --- %
% ------------------ %
accMin([], Min, Min).
accMin([H|T], Min, X) :- (H < Min, accMin(T, H, X)) ;
                         (H >= Min, accMin(T, Min, X)).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
scalarMult(_, [], _).
scalarMult(X, [H|T], [H1|T1]) :- H1 is H * X, scalarMult(X, T, T1).

% Tests
:- scalarMult(3, [2, 7, 4], Result),
   Result = [6, 21, 12].


% ------------------ %
% --- Question 3 --- %
% ------------------ %
accDot([], [], Dot, Dot).
accDot([H1|T1], [H2|T2], Dot, X) :- NewDot is Dot + H1 * H2,
                                    accDot(T1, T2, NewDot, X).
dot(X, Y, Z) :- accDot(X, Y, 0, Z).

% Tests
:- dot([2, 5, 6], [3, 4, 1], Result),
   Result = 32.
