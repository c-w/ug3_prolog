:- module(trees, [pptree/2]).

% ------------------ %
% --- Question 3 --- %
% ------------------ %
tab(_, 0).
tab(S, X) :- write(S, ' '), NewX is X - 1, tab(S, NewX).

pptree(S, s(NP, VP)) :- write(S, 's('), nl(S),
                        tab(S, 4), pptree(S, NP, 4), nl(S),
                        tab(S, 4), pptree(S, VP, 4),
                        write(S, ')').

pptree(S, np(DET, N), I) :- write(S, 'np('), nl(S), Ind is I + 4,
                            tab(S, Ind), pptree(S, DET, Ind), nl(S),
                            tab(S, Ind), pptree(S, N, Ind),
                            write(S, ')').

pptree(S, np(PRO), I) :- write(S, 'np('), nl(S), Ind is I + 4,
                         tab(S, Ind), pptree(S, PRO, Ind),
                         write(S, ')').

pptree(S, vp(V, NP), I) :- write(S, 'vp('), nl(S), Ind is I + 4,
                           tab(S, Ind), pptree(S, V, Ind), nl(S),
                           tab(S, Ind), pptree(S, NP, Ind),
                           write(S, ')').

pptree(S, vp(V), I) :- write(S, 'vp('), nl(S), Ind is I + 4,
                       tab(S, Ind), pptree(S, V, Ind),
                       write(S, ')').

pptree(S, Terminal, _) :- write(S, Terminal).
