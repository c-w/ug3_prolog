main :- open('hogwart.houses', write, Stream),
        write(Stream, '       '), write(Stream, 'gryffindor'), nl(Stream),
        write(Stream, 'hufflepuff'), write(Stream, '    '),
        write(Stream, 'ravenclaw'), nl(Stream),
        write(Stream, '       '), write(Stream, 'slytherin'), nl(Stream),
        close(Stream).
