:- dynamic word/2.

between(X, Lo, Hi) :- X >= Lo, X =< Hi.

lowercaseRec([], []) :- !.
lowercaseRec([U|T1], [L|T2]) :- (between(U, 65, 90) -> L is U + 32; L is U),
                                lowercaseRec(T1, T2), !.
lowercase(Upper, Lower) :- atom_codes(Upper, UpperChars),
                           lowercaseRec(UpperChars, LowerChars),
                           atom_codes(Lower, LowerChars).

readWord(Stream, Word, Status) :-
    get_code(Stream, Char),
    checkCharAndReadRest(Char, Chars, Stream, Status),
    atom_codes(Word, Chars).

checkCharAndReadRest(end_of_file, [], _, eof) :- !.
checkCharAndReadRest(-1, [], _, eof) :- !.
checkCharAndReadRest(Code, [], _, ok) :- \+(between(Code, 65, 90)),
                                         \+(between(Code, 97, 122)), !.
checkCharAndReadRest(Char, [Char|Chars], Stream, Status) :-
    get_code(Stream, NextChar),
    checkCharAndReadRest(NextChar, Chars, Stream, Status).

readWords(_, eof).
readWords(Stream, Status) :-
    \+(Status = eof),
    readWord(Stream, RawWord, NewStatus),
    lowercase(RawWord, Word),
    addWord(Word),
    readWords(Stream, NewStatus).

addWord(Word) :-
    (word(Word, OldCount), !
    -> retract(word(Word, OldCount)),
       Count is OldCount + 1
    ;  Count = 1),
    assert(word(Word, Count)).

readFile(Path) :-
    open(Path, read, Stream),
    readWords(Stream, yes),
    close(Stream).
