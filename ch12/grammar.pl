:- module(grammar, [s/3]).

% Grammar
s(s(NP, VP)) --> np(NP, Nb, subject), vp(VP, Nb, _).

np(np(DET, N), Nb, _) --> det(DET, Nb, _), n(N, Nb, _).
np(np(PRO), Nb, Fn) --> pro(PRO, Nb, Fn).

vp(vp(V, NP), Nb, _) --> v(V, Nb, _), np(NP, _, object).
vp(vp(V), Nb, _) --> v(V, Nb, _).

det(det(Word), Nb, _) --> [Word], {lex(Word, Nb, _, det)}.
v(v(Word), Nb, _) --> [Word], {lex(Word, Nb, _, v)}.
n(n(Word), Nb, _) --> [Word], {lex(Word, Nb, _, n)}.
pro(pro(Word), Nb, Fn) --> [Word], {lex(Word, Nb, Fn, pro)}.

% Lexicon
lex(the, _, _, det).
lex(a, singular, _, det).

lex(eat, plural, _, v).
lex(eats, singular, _, v).
lex(know, plural, _, v).
lex(knows, singular, _, v).
lex(shoots, singular, _, v).
lex(shoot, plural, _, v).

lex(women, plural, _, n).
lex(woman, singular, _, n).
lex(men, plural, _, n).
lex(man, singular, _, n).
lex(apples, plural, _, n).
lex(apple, singular, _, n).
lex(pears, plural, _, n).
lex(pear, singular, _, n).

lex(he, singular, subject, pro).
lex(she, singular, subject, pro).
lex(him, singular, object, pro).
lex(her, singular, object, pro).
