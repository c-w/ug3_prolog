% ------------------ %
% --- Question 1 --- %
% ------------------ %
:- ensure_loaded(grammar).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
:- ensure_loaded(trees).


% ------------------ %
% --- Question 4 --- %
% ------------------ %
testOne(List, File) :- s(Tree, List, []),
                       open(File, write, Stream),
                       pptree(Stream, Tree),
                       close(Stream).


% ------------------ %
% --- Question 5 --- %
% ------------------ %
testSent(end_of_file, _, _) :- !.
testSent(Sent, InStream, OutStream) :-
    write(OutStream, Sent), nl(OutStream), nl(OutStream),
    (s(Tree, Sent, [])
    -> pptree(OutStream, Tree)
    ;  write(OutStream, 'no')),
    read(InStream, NewSent),
    (NewSent = end_of_file
    -> !
    ; nl(OutStream), nl(OutStream), nl(OutStream)),
    testSent(NewSent, InStream, OutStream).

test(InFile, OutFile) :-
    open(InFile, read, InStream), open(OutFile, write, OutStream),
    read(InStream, Sent),
    testSent(Sent, InStream, OutStream),
    close(InStream), close(OutStream).
