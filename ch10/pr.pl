% ------------------ %
% --- Question 1 --- %
% ------------------ %
nuA(X, Y) :- \+(X = Y).

nuB(X, Y) :- X = Y, !, fail.
nuB(_, _).

nuC(X, X) :- !, fail.
nuC(_, _).

nu(X, Y) :- nuA(X, Y), nuB(X, Y), nuC(X, Y).

% Tests
:- \+(nu(foo, foo)).
:- nu(foo, blob).
:- \+(nu(foo, _X)).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
unifiable([], _, []) :- !.
unifiable([H|T], Term, [H|Nu]) :- \+(\+(H = Term)), !, unifiable(T, Term, Nu).
unifiable([_|T], Term, Nu) :- unifiable(T, Term, Nu), !.

% Tests
:- unifiable([X, b, t(Y)], t(a), List),
   List = [X, t(Y)].
