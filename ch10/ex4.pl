directTrain(saarbruecken, dudweiler).
directTrain(forbach, saarbruecken).
directTrain(freyming, forbach).
directTrain(stAvold, freyming).
directTrain(fahlquemont, stAvold).
directTrain(metz, fahlquemont).
directTrain(nancy, metz).

reachable(X, Y) :- directTrain(X, Y); directTrain(Y, X).

% base case: we can reach B from A in one step --- add to route and return
accRoute(A, B, R, [B|R]) :- reachable(A, B), \+(member(B, R)).
% recursion step: go to some place you can reach that you haven't gone to before
accRoute(A, B, R, Route) :- reachable(A, C), \+(member(C, R)),
                            accRoute(C, B, [C|R], Route).
% inverted top level call so we can use accumulators without reversing
route(From, To, Route) :- accRoute(To, From, [To], Route).

% Tests
:- route(forbach, metz, Route),
   Route = [forbach, freyming, stAvold, fahlquemont, metz].
