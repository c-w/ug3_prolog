% A number can only be positive, zero, or negative:
% any clause should thus prevent backtracking
class(Number, positive) :- Number > 0, !.
class(0, zero) :- !.
class(Number, negative) :- Number < 0, !.
