toptail([_|T], X) :- append(X, Y, T), length(Y, 1).

% Tests
:- \+(toptail([a], _)).
:- toptail([a, b], T),
   T = [].
:- toptail([a, b, c], T),
   T = [b].
