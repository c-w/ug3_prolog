% ------------------ %
% --- Question 1 --- %
% ------------------ %
member2(E, L) :- append(_, [E|_], L).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
reverse([], []).
reverse([H|T], X) :- reverse(T, RevT), append(RevT, [H], X).

accSet([], Acc, RevAcc) :- reverse(Acc, RevAcc).
accSet([H|T], Acc, R) :- member(H, Acc), accSet(T, Acc, R).
accSet([H|T], Acc, R) :- accSet(T, [H|Acc], R).
set([H|T], R) :- accSet(T, [H], R).

% Tests
:- set([2, 2, foo, 1, foo, [], []], X),
   X = [2, foo, 1, []].


% ------------------ %
% --- Question 3 --- %
% ------------------ %
not_list(X) :- X \= [], X \= [_|_].

accFlatten([], F, F).
accFlatten(X, F, [X|F]) :- not_list(X).
accFlatten([H|T], Acc, R) :- accFlatten(H, NewAcc, R),
                             accFlatten(T, Acc, NewAcc).
flatten(X, Y) :- accFlatten(X, [], Y).

% Tests
:- flatten([a, b, [c, d], [[1, 2]], foo], X),
   X = [a, b, c, d, 1, 2, foo].
:- flatten([a, b, [[[[[[[c, d]]]]]]], [[1, 2]], foo, []], X),
   X = [a, b, c, d, 1, 2, foo].
