doubled(List) :- append(X, X, List).

% Tests
:- doubled([a, b, c, a, b, c]).
:- doubled([foo, gubble, foo, gubble]).
:- \+(doubled([foo, gubble, foo])).
