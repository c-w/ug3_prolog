prefix(P, L) :- append(P, _, L).
suffix(S, L) :- append(_, S, L).
sublist(SubLi, Li) :- prefix(SubLi, Suf), suffix(Suf, Li).

zebra(N) :-
    Street = [_, _, _],
    member(house(red, _, _), Street),
    member(house(green, _, _), Street),
    member(house(blue, _, _), Street),
    % there is some house with a zebra
    member(house(_, N, zebra), Street),
    % constraint 1: the enlishman lives in the red house
    member(house(red, english, _), Street),
    % constraint 2: the jaguar is the pet of the spanish family
    member(house(_, spanish, jaguar), Street),
    % constraint 3: the japanese lives to the right of the snailkeeper
    sublist([house(_, _, snail), house(_, japanese, _)], Street),
    % constraint 4: the snail keeper lives to the left of the blue house
    sublist([house(_, _, snail), house(blue, _, _)], Street).
