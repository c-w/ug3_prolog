% ------------------ %
% --- Question 1 --- %
% ------------------ %
accRev([], X, X).
accRev([H|T], A, L) :- accRev(T, [H|A], L).
reverse(X, Y) :- accRev(X, [], Y).

lastAcc(List, X) :- reverse(List, [X|_]).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
lastRec([X], X).
lastRec([_|T], X) :- lastRec(T, X).
