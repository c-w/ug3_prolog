% ------------------ %
% --- Question 1 --- %
% ------------------ %
swapfl([H1|T1], [H2|T2]) :- append(C, [H2], T1), append(C, [H1], T2).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
swapflRec([La], [Fi], Fi, La).
swapflRec([H|T1], [H|T2], Fi, La) :- swapflRec(T1, T2, Fi, La).
swapflRec([H1|T1], [H2|T2]) :- swapflRec(T1, T2, H1, H2).
