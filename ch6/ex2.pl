accRev([], X, X).
accRev([H|T], A, L) :- accRev(T, [H|A], L).
reverse(X, Y) :- accRev(X, [], Y).

palindrome(List) :- reverse(List, List).

% Tests
:- palindrome([r, o, t, a, t, o, r]).
:- palindrome([n, u, r, s, e, s, r, u, n]).
:- \+(palindrome([n, o, t, t, h, i, s])).
