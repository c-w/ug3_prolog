% ------------------ %
% --- Question 1 --- %
% ------------------ %
tab(0).
tab(X) :- write(' '), NewX is X - 1, tab(NewX).

pptreeRec([], _).
pptreeRec([','|T], Ind) :- nl, tab(Ind), pptreeRec(T, Ind).
pptreeRec(['('|T], Ind) :- NewInd is Ind + 4,
                           write('('), nl, tab(NewInd),
                           pptreeRec(T, NewInd).
pptreeRec([')'|T], Ind) :- NewInd is Ind - 4,
                           nl, tab(NewInd), write(')'),
                           pptreeRec(T, NewInd).
pptreeRec([H|T], Ind) :- write(H),
                         pptreeRec(T, Ind).

pptree(Tree) :- atom_chars(Tree, Chars), pptreeRec(Chars, 0), nl.

% Tests
:- pptree('s(np(det(a),n(man)),vp(v(shoots),np(det(a),n(woman))))').


% ------------------ %
% --- Question 2 --- %
% ------------------ %
:- op(300, fx, not).
:- op(400, yfx, and).
:- op(500, yfx, or).
:- op(600, yfx, implies).

% Tests
:- display(not(p implies q)), nl.
:- display(not p implies q), nl.
