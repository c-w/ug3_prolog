termtype(Term, atom) :- atom(Term).
termtype(Term, number) :- number(Term).

termtype(Term, variable) :- var(Term).
termtype(Term, constant) :- atomic(Term).

termtype(Term, Type) :- (nonvar(Term), functor(Term, _, A), A > 0 ->
                            Type = complex_term;
                            Type = simple_term).

termtype(_, term).

% Tests
:- termtype(_Vincent, variable).
:- bagof(X, termtype(mia, X), Bag),
   Bag = ['atom', constant, simple_term, term].
:- bagof(X, termtype(dead(zed), X), Bag),
   Bag = [complex_term, term].
