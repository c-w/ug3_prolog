groundtermRec([]).
groundtermRec([H|T]) :- nonvar(H), groundtermRec(T).
groundterm(X) :- nonvar(X), X =.. Terms, groundtermRec(Terms).

% Tests
:- \+(groundterm(_X)).
:- groundterm(french(big_mac, le_bic_mac)).
:- \+(groundterm(french(whopper, _X))).
