% ------------------ %
% --- Question 1 --- %
% ------------------ %
even --> [].
even --> [a, a], even.

% Tests
:- even([], []).
:- even([a, a, a, a], []).
:- \+(even([a, a, a], [])).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
s --> n, m.
n --> [].
n --> [a], n, m, [d].
m --> [].
m --> [b, b], m, [c, c].

% Tests
:- s([], []).
:- s([a, b, b, c, c, d], []).
:- s([a, a, b, b, b, b, c, c, c, c, d, d], []).


% ------------------ %
% --- Question 3 --- %
% ------------------ %
prop --> [p].
prop --> [q].
prop --> [r].
prop --> [not], prop.
prop --> ['('], prop, [and], prop, [')'].
prop --> ['('], prop, [or], prop, [')'].
prop --> ['('], prop, [implies], prop, [')'].

% Tests
:- prop([not, '(', p, implies, q, ')'], []).
