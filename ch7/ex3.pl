s --> [].
s --> [a], s, [b, b].

% Tests
:- s([], []).
:- s([a, b, b], []).
:- s([a, a, b, b, b, b], []).
:- s([a, a, a, b ,b ,b, b, b, b], []).
