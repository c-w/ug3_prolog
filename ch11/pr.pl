% ------------------ %
% --- Question 1 --- %
% ------------------ %
subset([], []).
subset([H|T1], [H|T2]) :- subset(T1, T2).
subset(T1, [_|T2]) :- subset(T1, T2).

% Tests
:- subset([a, b], [a, b, c]).
:- subset([b, c], [a, b, c]).
:- subset([], [a, b, c]).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
powerset(Set, Pow) :- findall(X, subset(X, Set), Pow).

% Tests
:- powerset([a, b, c], PP), sort(PP, P),
   P = [[], [a], [a, b], [a, b, c], [a, c], [b], [b, c], [c]].
