:- dynamic sigmares/2.

sigma(1, 1) :- !.
sigma(N, Sum) :- sigmares(N, Sum), !.
sigma(N, Sum) :- NewN is N - 1, sigma(NewN, NewSum), Sum is NewSum + N,
                 assert(sigmares(N, Sum)), !.

% Tests
:- sigma(3, X),
   X = 6.
:- sigma(5, X),
   X = 15.
:- sigma(2, X),
   X = 3.
