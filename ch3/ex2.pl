directlyIn(natasha, irina).
directlyIn(olga, natasha).
directlyIn(katarina, olga).

in(X, Y) :- directlyIn(X, Y).
in(X, Y) :- directlyIn(Z, Y), in(X, Z).

% Tests
:- in(katarina, natasha).
:- \+(in(olga, katarina)).
