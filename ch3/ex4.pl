greater_than(succ(_), 0).
greater_than(succ(X), X).
greater_than(X, Y) :- X = succ(Z), greater_than(Z, Y).

% Tests
:- greater_than(succ(succ(succ(0))), succ(0)).
:- \+(greater_than(succ(succ(0)), succ(succ(succ(0))))).
