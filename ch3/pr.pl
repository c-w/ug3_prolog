% ------------------ %
% --- Question 1 --- %
% ------------------ %
connected( 1,  2).
connected( 3,  4).
connected( 5,  6).
connected( 7,  8).
connected( 9, 10).
connected(12, 13).
connected(13, 14).
connected(15, 16).
connected(17, 18).
connected(19, 20).
connected( 4,  1).
connected( 6,  3).
connected( 4,  7).
connected( 6, 11).
connected(14,  9).
connected(11, 15).
connected(16, 12).
connected(14, 17).
connected(16, 19).

path(X, Y) :- connected(X, Y).
path(X, Y) :- connected(X, Z), path(Z, Y).


% ------------------ %
% --- Question 2 --- %
% ------------------ %
byCar(auckland, hamilton).
byCar(hamilton, raglan).
byCar(valmont, saarbruecken).
byCar(valmont, metz).

byTrain(metz, frankfurt).
byTrain(saarbruecken, frankfurt).
byTrain(metz, paris).
byTrain(saarbruecken, paris).

byPlane(frankfurt, bangkok).
byPlane(frankfurt, singapore).
byPlane(paris, losAngeles).
byPlane(bangkok, auckland).
byPlane(singapore, auckland).
byPlane(losAngeles, auckland).

byAny(X, Y) :- byCar(X, Y); byTrain(X, Y); byPlane(X, Y).

travel(X, Y) :- byAny(X, Y).
travel(X, Y) :- byAny(X, Z), travel(Z, Y).

% Tests
:- travel(valmont, raglan).


% ------------------ %
% --- Question 3 --- %
% ------------------ %
travel(X, Y, go(X, Y)) :- byAny(X, Y).
travel(X, Y, go(X, Z, L)) :- byAny(X, Z), travel(Z, Y, L).


% ------------------ %
% --- Question 4 --- %
% ------------------ %
travel2(X, Y, byCar(X, Y)) :- byCar(X, Y).
travel2(X, Y, byTrain(X, Y)) :- byTrain(X, Y).
travel2(X, Y, byPlane(X, Y)) :- byPlane(X, Y).
travel2(X, Y, byCar(X, Z, L)) :- byCar(X, Z), travel2(Z, Y, L).
travel2(X, Y, byTrain(X, Z, L)) :- byTrain(X, Z), travel2(Z, Y, L).
travel2(X, Y, byPlane(X, Z, L)) :- byPlane(X, Z), travel2(Z, Y, L).
