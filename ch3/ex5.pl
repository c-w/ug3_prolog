leaf(1).
leaf(2).
leaf(4).

tree(leaf(_), leaf(_)).
tree(tree(_, _), leaf(_)).

swap(tree(leaf(X), leaf(Y)), tree(leaf(Y), leaf(X))).
swap(tree(tree(X, Y), leaf(L)), tree(leaf(L), T)) :- swap(tree(X, Y), T).

% Tests
:- swap(tree(tree(leaf(1), leaf(2)), leaf(4)), T),
   T = tree(leaf(4), tree(leaf(2), leaf(1))).
