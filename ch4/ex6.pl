twice([], []).
twice([H|T], [H,H|T1]) :- twice(T, T1).

% Tests
:- twice([a, 4, buggle], X),
   X = [a, a, 4, 4, buggle, buggle].
:- twice([1, 2, 1, 1], X),
   X = [1, 1, 2, 2, 1, 1, 1, 1].
