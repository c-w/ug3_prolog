% ------------------ %
% --- Question 1 --- %
% ------------------ %
combine1([], [], _).
combine1([H1|T1], [H2|T2], [H1,H2|T]) :- combine1(T1, T2, T).

% Tests
:- combine1([a, b, c], [1, 2, 3], X),
   X = [a, 1, b, 2, c, 3].
:- combine1([f, b, yip, yup], [glu, gla, gli, glo], Result),
   Result = [f, glu, b, gla, yip, gli, yup, glo].


% ------------------ %
% --- Question 2 --- %
% ------------------ %
combine2([], [], _).
combine2([H1|T1], [H2|T2], [[H1,H2]|T]) :- combine2(T1, T2, T).

% Tests
:- combine2([a, b, c], [1, 2, 3], X),
   X = [[a, 1], [b, 2], [c, 3]].
:- combine2([f, b, yip, yup], [glu, gla, gli, glo], Result),
   Result = [[f, glu], [b, gla], [yip, gli], [yup, glo]].


% ------------------ %
% --- Question 3 --- %
% ------------------ %
combine3([], [], _).
combine3([H1|T1], [H2|T2], [j(H1,H2)|T]) :- combine3(T1, T2, T).

% Tests
:- combine3([a, b, c], [1, 2, 3], X),
   X = [j(a, 1), j(b, 2), j(c, 3)].
:- combine3([f, b, yip, yup], [glu, gla, gli, glo], Result),
   Result = [j(f, glu), j(b, gla), j(yip, gli), j(yup, glo)].
